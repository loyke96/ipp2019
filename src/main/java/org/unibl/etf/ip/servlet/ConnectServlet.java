/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.servlet;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.unibl.etf.ip.facade.FacultyFacade;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
@WebServlet(name = "connect", urlPatterns = {"/connect"})
public class ConnectServlet extends HttpServlet {

    @EJB
    private FacultyFacade facultyFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user_id") != null) {
            request.setAttribute("facultyFacade", facultyFacade);
            getServletContext().getRequestDispatcher("/WEB-INF/connect.jsp").forward(request, response);
        } else {
            getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
