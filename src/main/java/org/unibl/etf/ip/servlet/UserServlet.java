/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.servlet;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.StudentFacade;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
@WebServlet(name = "user", urlPatterns = {"/user/*"})
public class UserServlet extends HttpServlet {

    @EJB
    private StudentFacade studentFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            String username = request.getPathInfo().substring(1);
            Student me = studentFacade.find(userId);
            Student user = studentFacade.find(username);
            if (user != null && (me.equals(user) || me.getStudentSet2().contains(user))) {
                request.setAttribute("user", user);
                request.setAttribute("count", studentFacade.numberOfRequests(userId));
                getServletContext().getRequestDispatcher("/WEB-INF/user.jsp").forward(request, response);
            } else {
                getServletContext().getRequestDispatcher("/WEB-INF/404.jsp").forward(request, response);
            }
        } else {
            response.sendRedirect("/");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
