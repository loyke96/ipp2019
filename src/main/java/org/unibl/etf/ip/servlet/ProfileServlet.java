/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.FacultyFacade;
import org.unibl.etf.ip.facade.StudentFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@MultipartConfig
@WebServlet(name = "profile", urlPatterns = {"/profile"})
public class ProfileServlet extends HttpServlet {

    @EJB
    private StudentFacade studentFacade;
    @EJB
    private FacultyFacade facultyFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user_id") != null) {
            request.setAttribute("facultyFacade", facultyFacade);
            request.setAttribute("studentFacade", studentFacade);
            getServletContext().getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
        } else {
            response.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student student = studentFacade.find(userId);
            Part part = request.getPart("profile_picture");
            if (part != null && part.getSize() != 0) {
                try (InputStream in = part.getInputStream()) {
                    String path = getServletContext().getRealPath("/img/" + student.getUsername() + ".jpg");
                    try (FileOutputStream out = new FileOutputStream(path, false)) {
                        byte[] buffer = new byte[512 * 1024];
                        int size;
                        while ((size = in.read(buffer)) != -1) {
                            out.write(buffer, 0, size);
                        }
                    }
                }
            }
            String param;
            if ((param = request.getParameter("name")) != null) {
                student.setName(param);
            }
            if ((param = request.getParameter("surname")) != null) {
                student.setSurname(param);
            }
            if ((param = request.getParameter("interests")) != null) {
                student.setInterests(param);
            }
            if ((param = request.getParameter("faculty")) != null) {
                student.setFaculty(facultyFacade.find(Integer.parseInt(param)));
            }
            if ((param = request.getParameter("study_field")) != null) {
                student.setStudyField(param);
            }
            if ((param = request.getParameter("study_year")) != null) {
                student.setStudyYear(Short.parseShort(param));
            }
            studentFacade.edit(student);
            doGet(request, response);
        } else {
            response.sendRedirect("/");
        }
    }

}
