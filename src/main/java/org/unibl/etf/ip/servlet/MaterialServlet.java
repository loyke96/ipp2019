/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.unibl.etf.ip.entity.Material;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.MaterialFacade;
import org.unibl.etf.ip.facade.StudentFacade;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
@MultipartConfig
@WebServlet(name = "material", urlPatterns = {"/material"})
public class MaterialServlet extends HttpServlet {

    @EJB
    private MaterialFacade materialFacade;
    @EJB
    private StudentFacade studentFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("materialFacade", materialFacade);
        getServletContext().getRequestDispatcher("/WEB-INF/material.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student student = studentFacade.find(userId);
            Part part = request.getPart("document");
            String description = request.getParameter("description");
            if (part != null && part.getSize() != 0 && description != null) {
                try (InputStream in = part.getInputStream()) {
                    String filename = System.currentTimeMillis() + "-" + part.getSubmittedFileName();
                    String path = getServletContext().getRealPath("/doc/" + filename);
                    try (FileOutputStream out = new FileOutputStream(path, false)) {
                        byte[] buffer = new byte[512 * 1024];
                        int size;
                        while ((size = in.read(buffer)) != -1) {
                            out.write(buffer, 0, size);
                        }
                        Material material = new Material();
                        material.setDescription(description);
                        material.setStudent(student);
                        material.setDocument(filename);
                        materialFacade.create(material);
                    }
                }
            }
            request.setAttribute("studentFacade", studentFacade);
            getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
        }
    }

}
