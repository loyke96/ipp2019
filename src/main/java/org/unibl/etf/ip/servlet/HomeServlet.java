package org.unibl.etf.ip.servlet;

import java.io.IOException;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.unibl.etf.ip.entity.Post;
import org.unibl.etf.ip.entity.UserPost;
import org.unibl.etf.ip.facade.PostFacade;
import org.unibl.etf.ip.facade.StudentFacade;
import org.unibl.etf.ip.facade.UserPostFacade;

@WebServlet(name = "home", urlPatterns = {"/home"})
public class HomeServlet extends HttpServlet {

    @EJB
    private StudentFacade studentFacade;
    @EJB
    private PostFacade postFacade;
    @EJB
    private UserPostFacade userPostFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user_id") != null) {
            request.setAttribute("studentFacade", studentFacade);
            getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
        } else {
            getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            String content = request.getParameter("content").trim();
            if (content.contains("youtube.com")) {
                content = content.replace("watch?v=", "embed/");
            }
            Post newPost = new Post(null, new Date());
            postFacade.create(newPost);
            UserPost newUserPost = new UserPost(newPost.getId(), content);
            newUserPost.setStudent(studentFacade.find(userId));
            userPostFacade.create(newUserPost);
        }
        doGet(request, response);
    }

}
