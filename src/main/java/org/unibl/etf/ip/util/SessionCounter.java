/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@WebListener
public class SessionCounter implements HttpSessionListener {

    private static HashMap<String, Integer> histogram = new HashMap<>();
    private static int count = 0;

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        String key = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddHH"));
        Integer count_ = histogram.get(key);
        histogram.put(key, count_ != null ? count_ + 1 : 1);
        ++count;
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        --count;
    }

    public static int getCount() {
        return count;
    }

    public static HashMap<String, Integer> getHistogram() {
        return histogram;
    }

}
