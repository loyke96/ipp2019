/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.rss;

import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndCategoryImpl;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedOutput;
import java.io.IOException;
import java.util.LinkedList;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.unibl.etf.ip.facade.EventFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@WebServlet(name = "EventRSS", urlPatterns = {"/rss/event"})
public class EventRSS extends HttpServlet {

    @EJB
    private EventFacade eventFacade;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SyndFeedImpl feed = new SyndFeedImpl();
        feed.setFeedType("rss_2.0");
        feed.setTitle("event-rss");
        feed.setDescription("Iinternet programiranje - RSS Feed");
        feed.setLink("http://example.org/rss/event");
        LinkedList<SyndEntry> entries = new LinkedList<>();
        eventFacade.findAll().forEach(e -> {
            SyndEntryImpl entry = new SyndEntryImpl();
            entry.setTitle(e.getTitle());
            SyndContentImpl content = new SyndContentImpl();
            content.setValue(e.getDescription());
            entry.setDescription(content);
            entry.setPublishedDate(e.getPost().getCreationDate());
            SyndCategoryImpl category = new SyndCategoryImpl();
            category.setName(e.getCategory().getName());
            LinkedList<SyndCategory> categories = new LinkedList<>();
            categories.add(category);
            entry.setCategories(categories);
            entries.add(entry);
        });
        feed.setEntries(entries);
        try {
            response.getWriter().println(new SyndFeedOutput().outputString(feed));
        } catch (FeedException ex) {
            response.sendError(500);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
