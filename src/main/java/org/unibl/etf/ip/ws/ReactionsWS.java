/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.ws;

import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.unibl.etf.ip.facade.ReactionFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Path("/reactions")
public class ReactionsWS {

    @Context
    private HttpServletRequest request;
    @EJB
    private ReactionFacade reactionFacade;

    @Path("/like")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response like(HashMap<String, Integer> body) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId == null) {
            return Response.status(401).build();
        }
        return Response.ok(reactionFacade.like(userId, body.get("postId"))).build();
    }

    @Path("/dislike")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response dislike(HashMap<String, Integer> body) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId == null) {
            return Response.status(401).build();
        }
        return Response.ok(reactionFacade.dislike(userId, body.get("postId"))).build();
    }

}
