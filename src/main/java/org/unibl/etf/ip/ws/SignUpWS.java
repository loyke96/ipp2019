/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.ws;

import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.StudentFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Path("/signup")
public class SignUpWS {

    @Context
    private HttpServletRequest request;

    @EJB
    private StudentFacade studentFacade;

    @POST
    @Path("/available")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response existsUsernameOrEmail(HashMap<String, String> request) {
        String username = request.get("username");
        String email = request.get("email");
        if (username == null || email == null) {
            return Response.status(400).build();
        } else {
            HashMap<String, Boolean> result = new HashMap<>();
            result.put("username", !studentFacade.usernameExists(username));
            result.put("email", !studentFacade.emailExists(email));
            return Response.ok(result).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signUp(Student student) {
        try {
            student.setBlock(false);
            studentFacade.create(student);
            System.out.println(student.getId());
            request.getSession().setAttribute("user_id", student.getId());
            return Response.status(200).build();
        } catch (Exception e) {
            return Response.status(400).build();
        }
    }

}
