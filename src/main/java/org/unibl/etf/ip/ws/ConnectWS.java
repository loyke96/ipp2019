package org.unibl.etf.ip.ws;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.StudentFacade;

@Path("/connect")
public class ConnectWS {

    @Context
    private HttpServletRequest request;
    @EJB
    private StudentFacade studentFacade;

    @GET
    @Path("/init")
    @Produces(MediaType.APPLICATION_JSON)
    public Response init() {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student me = studentFacade.find(userId);
            HashMap<String, Object> result = new HashMap<>();
            try {
                result.put("faculty", me.getFaculty().getId());
            } catch (Throwable t) {
                result.put("faculty", 0);
            }
            result.put("friends", me.getStudentSet2().parallelStream().map(s -> {
                return new HashMap<String, Object>() {
                    {
                        put("username", s.getUsername());
                        put("name", s.getName());
                        put("surname", s.getSurname());
                        try {
                            put("faculty", s.getFaculty().getId());
                        } catch (Throwable t) {
                            put("faculty", 0);
                        }
                    }
                };
            }).collect(Collectors.toSet()));
            result.put("requests", me.getStudentSet1().parallelStream().map(s -> {
                return new HashMap<String, Object>() {
                    {
                        put("username", s.getUsername());
                        put("name", s.getName());
                        put("surname", s.getSurname());
                        try {
                            put("faculty", s.getFaculty().getId());
                        } catch (Throwable t) {
                            put("faculty", 0);
                        }
                    }
                };
            }).collect(Collectors.toSet()));
            List<Student> others = studentFacade.findAll();
            others.remove(me);
            others.removeAll(me.getStudentSet2());
            others.removeAll(me.getStudentSet1());
            others.removeAll(me.getStudentSet());
            result.put("others", others.stream().map(s -> {
                return new HashMap<String, Object>() {
                    {
                        put("username", s.getUsername());
                        put("name", s.getName());
                        put("surname", s.getSurname());
                        try {
                            put("faculty", s.getFaculty().getId());
                        } catch (Throwable t) {
                            put("faculty", 0);
                        }
                    }
                };
            }).collect(Collectors.toSet()));
            return Response.ok(result).build();
        } else {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/accept")
    public Response accept(String username) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student me = studentFacade.find(userId);
            Student friend = studentFacade.find(username);
            friend.getStudentSet().remove(me);
            me.getStudentSet2().add(friend);
            friend.getStudentSet2().add(me);
            studentFacade.edit(me);
            studentFacade.edit(friend);
            return Response.ok().build();
        } else {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/decline")
    public Response decline(String username) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student whom = studentFacade.find(username);
            whom.getStudentSet().remove(studentFacade.find(userId));
            studentFacade.edit(whom);
            return Response.ok().build();
        } else {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/remove")
    public Response remove(String username) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student me = studentFacade.find(userId);
            Student other = studentFacade.find(username);
            me.getStudentSet2().remove(other);
            other.getStudentSet2().remove(me);
            studentFacade.edit(me);
            studentFacade.edit(other);
            return Response.ok().build();
        } else {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/request")
    public Response request(String username) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            Student me = studentFacade.find(userId);
            Student other = studentFacade.find(username);
            me.getStudentSet().add(other);
            studentFacade.edit(me);
            return Response.ok().build();
        } else {
            return Response.status(401).build();
        }
    }

}
