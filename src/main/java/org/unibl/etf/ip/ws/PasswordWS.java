/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.ws;

import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.StudentFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Path("/password")
public class PasswordWS {

    @Context
    private HttpServletRequest request;
    @EJB
    private StudentFacade studentFacade;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(HashMap<String, String> passwords) {
        Integer userId = (Integer) request.getSession().getAttribute("user_id");
        if (userId != null) {
            Student student = studentFacade.find(userId);
            String oldPassword = passwords.get("old");
            String newPassword = passwords.get("new");
            if (student.getPassword().equals(oldPassword)) {
                student.setPassword(newPassword);
                studentFacade.edit(student);
                return Response.status(200).build();
            } else {
                return Response.status(401).build();
            }
        } else {
            return Response.status(403).build();
        }
    }
}
