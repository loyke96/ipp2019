/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.ws;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@ApplicationPath("/api")
public class WsConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<>(
                Arrays.asList(
                        PostsWS.class,
                        ReactionsWS.class,
                        SignUpWS.class,
                        PasswordWS.class,
                        ConnectWS.class,
                        BlogWS.class
                )
        );
    }

}
