/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.ws;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.unibl.etf.ip.facade.PostFacade;
import org.unibl.etf.ip.facade.UserPostFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Path("/posts")
public class PostsWS {

    @EJB
    private PostFacade postFacade;

    @EJB
    private UserPostFacade userPostFacade;

    @Context
    private HttpServletRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPosts() {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            return Response.ok(postFacade.findPosts(userId)).build();
        } else {
            return Response.status(401).build();
        }
    }

    @GET
    @Path("/{user}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPosts(@PathParam("user") String user) {
        HttpSession session = request.getSession();
        Integer myId = (Integer) session.getAttribute("user_id");
        if (myId != null) {
            return Response.ok(userPostFacade.find(user, myId)).build();
        } else {
            return Response.status(401).build();
        }
    }

}
