package org.unibl.etf.ip.ws;

import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.unibl.etf.ip.facade.BlogFacade;
import org.unibl.etf.ip.facade.StudentFacade;

@Path("/blog")
public class BlogWS {

    @Context
    private HttpServletRequest request;
    @EJB
    private StudentFacade studentFacade;
    private static BlogFacade blogFacade = new BlogFacade();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findBlog() {
        return Response.ok(blogFacade.findBlog()).build();
    }

    @POST
    public Response newTopic(String content) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            String username = studentFacade.find(userId).getUsername();
            blogFacade.newTopic(username, content);
            return Response.ok().build();
        } else {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/comment")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response comment(HashMap<String, String> req) {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("user_id");
        if (userId != null) {
            blogFacade.addComment(studentFacade.find(userId).getUsername(), req.get("id"), req.get("content"));
            return Response.ok().build();
        } else {
            return Response.status(401).build();
        }
    }

}
