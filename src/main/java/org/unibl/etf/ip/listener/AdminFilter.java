/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.listener;

import javax.faces.annotation.FacesConfig;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Inject;
import org.unibl.etf.ip.bean.AdminBean;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@FacesConfig(version = FacesConfig.Version.JSF_2_3)
public class AdminFilter implements PhaseListener {

    @Inject
    private AdminBean adminBean;
    @Inject
    private UIViewRoot viewRoot;

    @Override
    public void afterPhase(PhaseEvent pe) {
    }

    @Override
    public void beforePhase(PhaseEvent pe) {
        if (adminBean.isLoggedIn() == false && viewRoot.getViewId().startsWith("/admin")) {
            viewRoot.setViewId("/admin/login.xhtml");
        } else if ("/admin/login.xhtml".equals(viewRoot.getViewId())) {
            viewRoot.setViewId("/admin/index.xhtml");
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

}
