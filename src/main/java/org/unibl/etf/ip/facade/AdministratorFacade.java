/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.unibl.etf.ip.entity.Administrator;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class AdministratorFacade extends AbstractFacade<Administrator> {

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdministratorFacade() {
        super(Administrator.class);
    }

    public boolean login(String username, String password) {
        try {
            Administrator admin = em
                    .createNamedQuery("Administrator.findByUsername", Administrator.class)
                    .setParameter("username", username)
                    .getSingleResult();
            return admin.getPassword().equals(password);
        } catch (Throwable t) {
            return false;
        }
    }

}
