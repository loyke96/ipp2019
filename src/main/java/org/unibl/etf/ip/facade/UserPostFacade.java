/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.unibl.etf.ip.entity.Reaction;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.entity.UserPost;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class UserPostFacade extends AbstractFacade<UserPost> {

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserPostFacade() {
        super(UserPost.class);
    }

    public Collection<Object> findUserPosts(int myId) {
        LinkedList<Object> result = new LinkedList<>();
        Student me = em.createNamedQuery("Student.findById", Student.class)
                .setParameter("id", myId)
                .getSingleResult();
        LinkedList<Student> authors = new LinkedList<>(me.getStudentSet2());
        authors.add(me);
        authors.forEach(s -> {
            result.addAll(
                    s.getUserPostSet()
                            .parallelStream()
                            .map(up -> {
                                Collection<Reaction> reacts = up.getPost().getReactionSet();
                                long likes = reacts.parallelStream().filter(r -> "like".equals(r.getReactionType())).count();
                                long dislikes = reacts.size() - likes;
                                Optional<Reaction> optReaction = reacts.parallelStream().filter(r -> r.getStudent1().getId() == myId).findFirst();
                                String myReaction = optReaction.isPresent() ? optReaction.get().getReactionType() : "none";
                                return new HashMap<String, Object>() {
                                    {
                                        put("id", up.getId());
                                        put("type", "user_post");
                                        put("created", up.getPost().getCreationDate());
                                        put("content", up.getContent());
                                        put("author", up.getStudent().getName() + " " + up.getStudent().getSurname());
                                        put("authorUsername", up.getStudent().getUsername());
                                        put("likes", likes);
                                        put("dislikes", dislikes);
                                        put("myReaction", myReaction);
                                    }
                                };
                            }).collect(Collectors.toSet())
            );
        });
        return result;
    }

    public Collection<Object> find(String username, int myId) {
        Student author = em.createNamedQuery("Student.findByUsername", Student.class)
                .setParameter("username", username)
                .getSingleResult();
        return author.getUserPostSet()
                .parallelStream()
                .map(up -> {
                    Collection<Reaction> reacts = up.getPost().getReactionSet();
                    long likes = reacts.parallelStream().filter(r -> "like".equals(r.getReactionType())).count();
                    long dislikes = reacts.size() - likes;
                    Optional<Reaction> optReaction = reacts.parallelStream().filter(r -> r.getStudent1().getId() == myId).findFirst();
                    String myReaction = optReaction.isPresent() ? optReaction.get().getReactionType() : "none";
                    return new HashMap<String, Object>() {
                        {
                            put("id", up.getId());
                            put("type", "user_post");
                            put("created", up.getPost().getCreationDate());
                            put("content", up.getContent());
                            put("author", up.getStudent().getName() + " " + up.getStudent().getSurname());
                            put("authorUsername", up.getStudent().getUsername());
                            put("likes", likes);
                            put("dislikes", dislikes);
                            put("myReaction", myReaction);
                        }
                    };
                }).collect(Collectors.toSet());
    }
}
