/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.unibl.etf.ip.entity.Reaction;
import org.unibl.etf.ip.entity.ReactionPK;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class ReactionFacade extends AbstractFacade<Reaction> {

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReactionFacade() {
        super(Reaction.class);
    }

    public int like(int userId, int postId) {
        ReactionPK reactionPK = new ReactionPK(userId, postId);
        Reaction reaction = find(reactionPK);
        if (reaction == null) {
            create(new Reaction(reactionPK, "like"));
            return 1;
        } else if ("like".equals(reaction.getReactionType())) {
            remove(reaction);
            return -1;
        } else {
            reaction.setReactionType("like");
            edit(reaction);
            return 2;
        }
    }

    public int dislike(int userId, int postId) {
        ReactionPK reactionPK = new ReactionPK(userId, postId);
        Reaction reaction = find(reactionPK);
        if (reaction == null) {
            create(new Reaction(reactionPK, "dislike"));
            return 1;
        } else if ("dislike".equals(reaction.getReactionType())) {
            remove(reaction);
            return -1;
        } else {
            reaction.setReactionType("dislike");
            edit(reaction);
            return 2;
        }
    }
}
