package org.unibl.etf.ip.facade;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;

public class BlogFacade {

    MongoCollection<Document> collection;

    public BlogFacade() {
        try {
            InputStream stream = getClass().getClassLoader().getResourceAsStream("../../WEB-INF/mongo.xml");
            Properties props = new Properties();
            props.loadFromXML(stream);
            collection = new MongoClient(props.getProperty("HOST"),
                    Integer.parseInt(props.getProperty("PORT")))
                    .getDatabase("ipp2019")
                    .getCollection("blog");
        } catch (IOException ex) {
            Logger.getLogger(BlogFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public HashSet<HashMap<String, Object>> findBlog() {
        HashSet<HashMap<String, Object>> result = new HashSet<>();
        MongoCursor<Document> cursor = collection.find().cursor();
        while (cursor.hasNext()) {
            Document next = cursor.next();
            HashMap<String, Object> item = new HashMap<>();
            item.put("id", next.get("_id", ObjectId.class).toHexString());
            item.put("author", next.get("author"));
            item.put("date", next.get("date"));
            item.put("content", next.get("content"));
            item.put("comments", next.get("comments"));
            result.add(item);
        }
        return result;
    }

    public void addComment(String author, String id, String content) {
        collection.findOneAndUpdate(new Document() {
            {
                put("_id", new ObjectId(id));
            }
        }, new Document() {
            {
                put("$push", new Document() {
                    {
                        put("comments", new Document() {
                            {
                                put("author", author);
                                put("date", new Date());
                                put("content", content);
                            }
                        });
                    }
                });
            }
        });
    }

    public void newTopic(String author, String content) {
        collection.insertOne(new Document() {
            {
                put("author", author);
                put("content", content);
                put("date", new Date());
            }
        });
    }

}
