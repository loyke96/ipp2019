/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.security.auth.login.LoginException;
import org.unibl.etf.ip.entity.Student;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class StudentFacade extends AbstractFacade<Student> {

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StudentFacade() {
        super(Student.class);
    }

    public Integer login(String username, String password) {
        try {
            Student student = em
                    .createNamedQuery("Student.findByUsername", Student.class)
                    .setParameter("username", username)
                    .getSingleResult();
            if (student.getBlock() == false && student.getPassword().equals(password)) {
                return student.getId();
            } else {
                throw new LoginException();
            }
        } catch (Throwable t) {
            return null;
        }
    }

    public Collection<Student> findConnections(int userId) {
        return em
                .createNamedQuery("Student.findById", Student.class)
                .setParameter("id", userId)
                .getSingleResult().getStudentSet2();
    }

    public boolean usernameExists(String username) {
        try {
            em.createNamedQuery("Student.findByUsername", Student.class)
                    .setParameter("username", username)
                    .getSingleResult();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean emailExists(String email) {
        try {
            em.createNamedQuery("Student.findByEmail", Student.class)
                    .setParameter("email", email)
                    .getSingleResult();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Student find(String username) {
        try {
            return em.createNamedQuery("Student.findByUsername", Student.class)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public int numberOfRequests(int id) {
        return find(id).getStudentSet1().size();
    }

}
