/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.unibl.etf.ip.entity.Post;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class PostFacade extends AbstractFacade<Post> {

    @EJB
    private NewsFacade newsFacade;
    @EJB
    private UserPostFacade userPostFacade;
    @EJB
    private EventFacade eventFacade;

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PostFacade() {
        super(Post.class);
    }

    public Collection<Object> findPosts(int userId) {
        Collection<Object> posts = userPostFacade.findUserPosts(userId);
        posts.addAll(newsFacade.findNews(userId));
        posts.addAll(eventFacade.findEvents(userId));
        return posts;
    }

}
