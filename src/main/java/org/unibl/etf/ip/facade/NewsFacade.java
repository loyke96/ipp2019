/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.unibl.etf.ip.entity.News;
import org.unibl.etf.ip.entity.Reaction;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class NewsFacade extends AbstractFacade<News> {

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NewsFacade() {
        super(News.class);
    }

    public Collection<Object> findNews(int userId) {
        return em.createNamedQuery("News.findAll", News.class)
                .getResultList().parallelStream().map(n -> {
                    Collection<Reaction> reacts = n.getPost().getReactionSet();
                    long likes = reacts.parallelStream().filter(r -> "like".equals(r.getReactionType())).count();
                    long dislikes = reacts.size() - likes;
                    Optional<Reaction> optReaction = reacts.parallelStream().filter(r -> r.getStudent1().getId() == userId).findFirst();
                    String myReaction = optReaction.isPresent() ? optReaction.get().getReactionType() : "none";
                    return new HashMap<String, Object>() {
                        {
                            put("id", n.getId());
                            put("type", "news");
                            put("created", n.getPost().getCreationDate());
                            put("content", n.getContent());
                            put("title", n.getTitle());
                            put("likes", likes);
                            put("dislikes", dislikes);
                            put("myReaction", myReaction);
                        }
                    };
                }).collect(Collectors.toSet());
    }

}
