/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.unibl.etf.ip.entity.Faculty;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Stateless
public class FacultyFacade extends AbstractFacade<Faculty> {

    @PersistenceContext(unitName = "ipp2019PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FacultyFacade() {
        super(Faculty.class);
    }
    
}
