/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.unibl.etf.ip.entity.Event;
import org.unibl.etf.ip.facade.EventFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "eventBean")
@ViewScoped
public class EventBean implements Serializable {

    @EJB
    private EventFacade eventFacade;
    private List<Event> events;
    private Event selected;

    @PostConstruct
    private void init() {
        events = eventFacade.findAll();
    }

    public List<Event> getEvents() {
        return events;
    }

    public Event getSelected() {
        return selected;
    }

    public void setSelected(Event selected) {
        this.selected = selected;
    }

    public void delete() {
        eventFacade.remove(selected);
        events.remove(selected);
    }

}
