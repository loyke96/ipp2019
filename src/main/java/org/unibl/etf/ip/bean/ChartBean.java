/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.unibl.etf.ip.util.SessionCounter;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "chartBean")
@ViewScoped
public class ChartBean implements Serializable {

    private final int[] last24h = new int[24];
    private LocalDateTime begin;
    private LineChartModel chart;

    @PostConstruct
    public void init() {
        getLast24h();
        initLinearModel();
    }

    public LineChartModel getChart() {
        return chart;
    }

    private void initLinearModel() {
        LineChartSeries lcs = new LineChartSeries();
        lcs.setLabel("Active");
        for (int i = 0; i < 24; ++i) {
            begin = begin.plusHours(1);
            lcs.set(begin.format(DateTimeFormatter.ofPattern("y-M-d H:00")), last24h[i]);
        }

        DateAxis dateAxis = new DateAxis();
        chart = new LineChartModel();
        chart.addSeries(lcs);
        chart.setTitle("Users");
        chart.setLegendPosition("e");
        chart.getAxes().put(AxisType.X, dateAxis);
    }

    public int[] getLast24h() {
        HashMap<String, Integer> histogram = SessionCounter.getHistogram();
        LocalDateTime time = begin = LocalDateTime.now().minusHours(24);
        for (int i = 0; i < 24; ++i) {
            time = time.plusHours(1);
            String key = time.format(DateTimeFormatter.ofPattern("yyMMddHH"));
            Integer value = histogram.get(key);
            last24h[i] = (value != null ? value : 0);
        }
        return last24h;
    }

}
