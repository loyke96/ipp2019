/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.unibl.etf.ip.entity.News;
import org.unibl.etf.ip.facade.NewsFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "newsBean")
@ViewScoped
public class NewsBean implements Serializable {

    @EJB
    private NewsFacade newsFacade;
    private List<News> news;
    private News selected;

    @PostConstruct
    private void init() {
        news = newsFacade.findAll();
    }

    public List<News> getNews() {
        return news;
    }

    public News getSelected() {
        return selected;
    }

    public void setSelected(News selected) {
        this.selected = selected;
    }

    public void delete() {
        newsFacade.remove(selected);
        news.remove(selected);
    }
}
