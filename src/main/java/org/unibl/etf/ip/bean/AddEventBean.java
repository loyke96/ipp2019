/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.annotation.FacesConfig;
import javax.faces.context.ExternalContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import org.unibl.etf.ip.entity.Event;
import org.unibl.etf.ip.entity.EventCategory;
import org.unibl.etf.ip.entity.Post;
import org.unibl.etf.ip.facade.EventFacade;
import org.unibl.etf.ip.facade.PostFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@FacesConfig(version = FacesConfig.Version.JSF_2_3)
@Named(value = "addEventBean")
@ViewScoped
public class AddEventBean implements Serializable {

    @Inject
    private ExternalContext externalContext;
    @EJB
    private EventFacade eventFacade;
    @EJB
    private PostFacade postFacade;
    private Event event;
    private boolean saved;
    private UploadedFile file;

    public AddEventBean() {
        event = new Event();
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getTitle() {
        return event.getTitle();
    }

    public void setTitle(String title) {
        event.setTitle(title);
    }

    public Date getHeldDate() {
        return event.getHeldDate();
    }

    public void setHeldDate(Date heldDate) {
        event.setHeldDate(heldDate);
    }

    public String getDescription() {
        return event.getDescription();
    }

    public void setDescription(String description) {
        event.setDescription(description);
    }

    public int getCategory() {
        try {
            return event.getCategory().getId();
        } catch (NullPointerException e) {
            setCategory(1);
            return 1;
        }
    }

    public void setCategory(int category) {
        event.setCategory(new EventCategory(category));
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public void create() throws IOException {
        if (file != null) {
            byte[] content = file.getContents();
            String fileName = System.currentTimeMillis() + "-" + file.getFileName();
            String realPath = externalContext.getRealPath("/event-pic/" + fileName);
            try (FileOutputStream out = new FileOutputStream(realPath, false)) {
                out.write(content);
            }
            event.setPicture(fileName);
        }
        Post post = new Post(null, new Date());
        postFacade.create(post);
        event.setId(post.getId());
        eventFacade.create(event);
        saved = true;
        event = new Event();
    }

}
