/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import org.unibl.etf.ip.facade.AdministratorFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "adminBean")
@SessionScoped
public class AdminBean implements Serializable {

    @EJB
    private AdministratorFacade administratorFacade;

    private String username;
    private String password;
    private boolean loggedIn = false;
    private boolean wrong = false;

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public boolean isWrong() {
        return wrong;
    }

    public String submit() {
        if (administratorFacade.login(username, password)) {
            loggedIn = true;
            wrong = false;
            return "index.xhtml";
        } else {
            loggedIn = false;
            wrong = true;
            return null;
        }
    }

    public String logOut() {
        loggedIn = false;
        return "login.xhtml";
    }
}
