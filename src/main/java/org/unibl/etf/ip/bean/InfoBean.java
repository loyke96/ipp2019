/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.unibl.etf.ip.facade.StudentFacade;
import org.unibl.etf.ip.util.SessionCounter;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "infoBean")
@ApplicationScoped
public class InfoBean {

    @EJB
    private StudentFacade studentFacade;

    public int getActiveUsers() {
        return SessionCounter.getCount();
    }

    public int getRegisteredUsers() {
        return studentFacade.count();
    }

}
