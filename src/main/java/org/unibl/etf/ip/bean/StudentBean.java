/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import org.apache.commons.lang3.RandomStringUtils;
import org.unibl.etf.ip.entity.Student;
import org.unibl.etf.ip.facade.StudentFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "studentBean")
@ViewScoped
public class StudentBean implements Serializable {

    @EJB
    private StudentFacade studentFacade;
    private List<Student> students;
    private Student selected;

    @PostConstruct
    public void init() {
        students = studentFacade.findAll();
    }

    public List<Student> getStudents() {
        return students;
    }

    public Student getSelected() {
        return selected;
    }

    public void setSelected(Student selected) {
        this.selected = selected;
    }

    public void block() {
        selected.setBlock(true);
        studentFacade.edit(selected);
    }

    public void unblock() {
        selected.setBlock(false);
        studentFacade.edit(selected);
    }

    public void resetPassword() {
        selected.setPassword(RandomStringUtils.random(8, true, true));
        studentFacade.edit(selected);

    }

}
