/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.bean;

import java.util.Date;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.unibl.etf.ip.entity.News;
import org.unibl.etf.ip.entity.Post;
import org.unibl.etf.ip.facade.NewsFacade;
import org.unibl.etf.ip.facade.PostFacade;

/**
 *
 * @author Željko Lojić <zeljko.lojic@yandex.com>
 */
@Named(value = "addNewsBean")
@RequestScoped
public class AddNewsBean {

    @EJB
    private NewsFacade newsFacade;
    @EJB
    private PostFacade postFacade;
    private News news;
    private boolean saved = false;

    public AddNewsBean() {
        news = new News();
    }

    public String getTitle() {
        return news.getTitle();
    }

    public void setTitle(String title) {
        news.setTitle(title);
    }

    public String getContent() {
        return news.getContent();
    }

    public void setContent(String content) {
        news.setContent(content);
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public void create() {
        Post post = new Post(null, new Date());
        postFacade.create(post);
        news.setId(post.getId());
        newsFacade.create(news);
        saved = true;
    }

}
