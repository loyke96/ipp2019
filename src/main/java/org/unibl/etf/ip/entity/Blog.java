/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.entity;

import java.util.HashMap;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
public class Blog {

    private String author;
    private String content;
    private HashMap<Integer, BlogComment> comments;

    public Blog() {
    }

    public Blog(String author, String content, HashMap<Integer, BlogComment> comments) {
        this.author = author;
        this.content = content;
        this.comments = comments;
    }

    public String getAuthor() {
        return author;
    }

    public HashMap<Integer, BlogComment> getComments() {
        return comments;
    }

    public String getContent() {
        return content;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setComments(HashMap<Integer, BlogComment> comments) {
        this.comments = comments;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
