/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.entity;

import java.util.Date;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
public class BlogComment {

    private String author;
    private String content;
    private Date date;

    public BlogComment() {
    }

    public BlogComment(String author, String content, Date date) {
        this.date = date;
        this.author = author;
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
