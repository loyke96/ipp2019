/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
@Entity
@Table(name = "student")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s"),
    @NamedQuery(name = "Student.findById", query = "SELECT s FROM Student s WHERE s.id = :id"),
    @NamedQuery(name = "Student.findByName", query = "SELECT s FROM Student s WHERE s.name = :name"),
    @NamedQuery(name = "Student.findBySurname", query = "SELECT s FROM Student s WHERE s.surname = :surname"),
    @NamedQuery(name = "Student.findByUsername", query = "SELECT s FROM Student s WHERE s.username = :username"),
    @NamedQuery(name = "Student.findByPassword", query = "SELECT s FROM Student s WHERE s.password = :password"),
    @NamedQuery(name = "Student.findByEmail", query = "SELECT s FROM Student s WHERE s.email = :email"),
    @NamedQuery(name = "Student.findByStudyField", query = "SELECT s FROM Student s WHERE s.studyField = :studyField"),
    @NamedQuery(name = "Student.findByStudyYear", query = "SELECT s FROM Student s WHERE s.studyYear = :studyYear"),
    @NamedQuery(name = "Student.findByBlock", query = "SELECT s FROM Student s WHERE s.block = :block")})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "surname")
    private String surname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    private String password;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "study_field")
    private String studyField;
    @Column(name = "study_year")
    private Short studyYear;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "interests")
    private String interests;
    @Column(name = "block")
    private Boolean block;
    @JoinTable(name = "connection_request", joinColumns = {
        @JoinColumn(name = "who", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "whom", referencedColumnName = "id")})
    @ManyToMany
    private Set<Student> studentSet;
    @ManyToMany(mappedBy = "studentSet")
    private Set<Student> studentSet1;
    @JoinTable(name = "connection", joinColumns = {
        @JoinColumn(name = "student1", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "student2", referencedColumnName = "id")})
    @ManyToMany
    private Set<Student> studentSet2;
    @ManyToMany(mappedBy = "studentSet2")
    private Set<Student> studentSet3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student1")
    private Set<Reaction> reactionSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<Material> materialSet;
    @JoinColumn(name = "faculty", referencedColumnName = "id")
    @ManyToOne
    private Faculty faculty;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<UserPost> userPostSet;

    public Student() {
    }

    public Student(Integer id) {
        this.id = id;
    }

    public Student(Integer id, String name, String surname, String username, String password, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStudyField() {
        return studyField;
    }

    public void setStudyField(String studyField) {
        this.studyField = studyField;
    }

    public Short getStudyYear() {
        return studyYear;
    }

    public void setStudyYear(Short studyYear) {
        this.studyYear = studyYear;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    @XmlTransient
    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Student> studentSet) {
        this.studentSet = studentSet;
    }

    @XmlTransient
    public Set<Student> getStudentSet1() {
        return studentSet1;
    }

    public void setStudentSet1(Set<Student> studentSet1) {
        this.studentSet1 = studentSet1;
    }

    @XmlTransient
    public Set<Student> getStudentSet2() {
        return studentSet2;
    }

    public void setStudentSet2(Set<Student> studentSet2) {
        this.studentSet2 = studentSet2;
    }

    @XmlTransient
    public Set<Student> getStudentSet3() {
        return studentSet3;
    }

    public void setStudentSet3(Set<Student> studentSet3) {
        this.studentSet3 = studentSet3;
    }

    @XmlTransient
    public Set<Reaction> getReactionSet() {
        return reactionSet;
    }

    public void setReactionSet(Set<Reaction> reactionSet) {
        this.reactionSet = reactionSet;
    }

    @XmlTransient
    public Set<Material> getMaterialSet() {
        return materialSet;
    }

    public void setMaterialSet(Set<Material> materialSet) {
        this.materialSet = materialSet;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @XmlTransient
    public Set<UserPost> getUserPostSet() {
        return userPostSet;
    }

    public void setUserPostSet(Set<UserPost> userPostSet) {
        this.userPostSet = userPostSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.unibl.etf.ip.entity.Student[ id=" + id + " ]";
    }

}
