/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
@Embeddable
public class ReactionPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "student")
    private int student;
    @Basic(optional = false)
    @NotNull
    @Column(name = "post")
    private int post;

    public ReactionPK() {
    }

    public ReactionPK(int student, int post) {
        this.student = student;
        this.post = post;
    }

    public int getStudent() {
        return student;
    }

    public void setStudent(int student) {
        this.student = student;
    }

    public int getPost() {
        return post;
    }

    public void setPost(int post) {
        this.post = post;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) student;
        hash += (int) post;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReactionPK)) {
            return false;
        }
        ReactionPK other = (ReactionPK) object;
        if (this.student != other.student) {
            return false;
        }
        if (this.post != other.post) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.unibl.etf.ip.entity.ReactionPK[ student=" + student + ", post=" + post + " ]";
    }
    
}
