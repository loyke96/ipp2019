/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unibl.etf.ip.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Željko Lojić<zeljko.lojic@yandex.com>
 */
@Entity
@Table(name = "reaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reaction.findAll", query = "SELECT r FROM Reaction r"),
    @NamedQuery(name = "Reaction.findByStudent", query = "SELECT r FROM Reaction r WHERE r.reactionPK.student = :student"),
    @NamedQuery(name = "Reaction.findByPost", query = "SELECT r FROM Reaction r WHERE r.reactionPK.post = :post"),
    @NamedQuery(name = "Reaction.findByReactionType", query = "SELECT r FROM Reaction r WHERE r.reactionType = :reactionType")})
public class Reaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReactionPK reactionPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "reaction_type")
    private String reactionType;
    @JoinColumn(name = "post", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Post post1;
    @JoinColumn(name = "student", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Student student1;

    public Reaction() {
    }

    public Reaction(ReactionPK reactionPK) {
        this.reactionPK = reactionPK;
    }

    public Reaction(ReactionPK reactionPK, String reactionType) {
        this.reactionPK = reactionPK;
        this.reactionType = reactionType;
    }

    public Reaction(int student, int post) {
        this.reactionPK = new ReactionPK(student, post);
    }

    public ReactionPK getReactionPK() {
        return reactionPK;
    }

    public void setReactionPK(ReactionPK reactionPK) {
        this.reactionPK = reactionPK;
    }

    public String getReactionType() {
        return reactionType;
    }

    public void setReactionType(String reactionType) {
        this.reactionType = reactionType;
    }

    public Post getPost1() {
        return post1;
    }

    public void setPost1(Post post1) {
        this.post1 = post1;
    }

    public Student getStudent1() {
        return student1;
    }

    public void setStudent1(Student student1) {
        this.student1 = student1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reactionPK != null ? reactionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reaction)) {
            return false;
        }
        Reaction other = (Reaction) object;
        if ((this.reactionPK == null && other.reactionPK != null) || (this.reactionPK != null && !this.reactionPK.equals(other.reactionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.unibl.etf.ip.entity.Reaction[ reactionPK=" + reactionPK + " ]";
    }
    
}
