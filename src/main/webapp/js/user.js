function get_posts() {
    const pathname = window.location.pathname.replace('/user/', '/api/posts/');
    $.get({
        url: pathname,
        success: function ($response) {
            const $post_area = $('#post-area');
            $post_area.empty();
            $.each($response, function (idx, elem) {
                const $post = $('<div class="border-bottom border-top mb-5">');
                $post_area.append($post);
                $post.append($('<a class="btn btn-sm btn-block text-left border-bottom bg-light">')
                        .attr('href', `/user/${elem.authorUsername}`)
                        .append($('<img class="profile-img">')
                                .attr('src', `/img/${elem.authorUsername}.jpg`)
                                .attr('onerror', 'this.src="/img/generic-user.png"'))
                        .append(elem.author));
                if (elem.content.match(/^https?:\/\//) === null) {
                    $post.append($('<p class="btn-sm btn-block">')
                            .text(elem.content));
                } else if (elem.content.match(/youtube\.com\/embed\//) === null) {
                    const parsedUrl = elem.content.split('/');
                    $post.append($('<a class="btn btn-sm btn-block text-left bg-light text-strong">')
                            .attr('href', elem.content)
                            .append($('<img class="w-25">')
                                    .attr('src', `${parsedUrl[0]}//${parsedUrl[2]}/favicon.ico`)
                                    .attr('onerror', 'this.src="/img/generic-site.png"'))
                            .append("  ")
                            .append(parsedUrl[2]));
                } else {
                    $post.append($('<div class="embed-responsive embed-responsive-16by9">')
                            .append($('<iframe class="embed-responsive-item" allowfullscreen>')
                                    .attr('src', elem.content)));
                }
                $post.append($('<div class="row m-1">')
                        .append($('<div class="col-2 text-center btn">')
                                .attr('onclick', `like(this,${elem.id})`)
                                .addClass(elem.myReaction === "like" ? "text-primary" : "")
                                .append($('<i class="fas fa-thumbs-up">'))
                                .append(elem.likes))
                        .append($('<div class="col-2 text-center btn">')
                                .attr('onclick', `dislike(this,${elem.id})`)
                                .addClass(elem.myReaction === "dislike" ? "text-danger" : "")
                                .append($('<i class="fas fa-thumbs-down">'))
                                .append(elem.dislikes))
                        .append($('<div class="col-8 text-center text-muted btn-sm">')
                                .append($('<i class="fas fa-clock">'))
                                .append(new Date(elem.created).toLocaleString('bs-ba'))));
            });
        }
    });
}

function like(node, id) {
    $.post({
        url: '/api/reactions/like',
        contentType: 'application/json',
        data: JSON.stringify({
            postId: id
        }),
        success: function ($response) {
            const $node = $(node);
            const counter = $node.text();
            if ($response === 1) {
                $node.addClass('text-primary');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-up"></i>${Number(counter) + 1}`);
            } else if ($response === -1) {
                $node.removeClass('text-primary');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-up"></i>${Number(counter) - 1}`);
            } else {
                const $nextNode = $node.next();
                const dislikeCounter = $nextNode.text();
                $node.addClass('text-primary');
                $nextNode.removeClass('text-danger');
                $node.empty();
                $nextNode.empty();
                $node.html(`<i class="fas fa-thumbs-up"></i>${Number(counter) + 1}`);
                $nextNode.html(`<i class="fas fa-thumbs-down"></i>${Number(dislikeCounter) - 1}`);
            }
        }
    });
}

function dislike(node, id) {
    $.post({
        url: '/api/reactions/dislike',
        contentType: 'application/json',
        data: JSON.stringify({
            postId: id
        }),
        success: function ($response) {
            const $node = $(node);
            const counter = $node.text();
            if ($response === 1) {
                $node.addClass('text-danger');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-down"></i>${Number(counter) + 1}`);
            } else if ($response === -1) {
                $node.removeClass('text-danger');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-down"></i>${Number(counter) - 1}`);
            } else {
                const $prevNode = $node.prev();
                const likeCounter = $prevNode.text();
                $node.addClass('text-danger');
                $prevNode.removeClass('text-primary');
                $node.empty();
                $prevNode.empty();
                $node.html(`<i class="fas fa-thumbs-down"></i>${Number(counter) + 1}`);
                $prevNode.html(`<i class="fas fa-thumbs-up"></i>${Number(likeCounter) - 1}`);
            }
        }
    });
}
