let popular = [];
let latest = [];
const rand = Date.now();
function fill_popular_area() {
    const $post_area = $('#popular-area');
    $post_area.empty();
    $.each(popular, function (idx, elem) {
        const $post = $('<div class="border-bottom border-top mb-5">');
        $post_area.append($post);
        if (elem.type === 'event') {
            $post.append($('<div class="card">')
                    .append($('<img class="card-img-top">')
                            .attr('src', `/event-pic/${elem.picture}`))
                    .append($('<div class="btn btn-lg btn-block border-bottom m-0">')
                            .append($('<div class="float-left">')
                                    .text(elem.title))
                            .append($('<div class="float-right">')
                                    .text(elem.category)))
                    .append($('<div class="card-body">')
                            .append($('<p class="card-text">')
                                    .text(elem.description))
                            .append($('<small>')
                                    .text(`Date: ${new Date(elem.held).toLocaleString('bs-ba')}`))));
        } else {
            if (elem.type === 'user_post') {
                $post.append($('<a class="btn btn-sm btn-block text-left border-bottom bg-light">')
                        .attr('href', `/user/${elem.authorUsername}`)
                        .append($('<img class="profile-img rounded-circle">')
                                .attr('src', `/img/${elem.authorUsername}.jpg?dummy=${rand}`)
                                .attr('onerror', 'this.src="/img/generic-user.png"'))
                        .append(elem.author));
            } else {
                $post.append($('<p class="btn btn-sm btn-block text-left border-bottom bg-light">')
                        .append($('<i class="fas fa-newspaper h3">'))
                        .append(elem.title));
            }
            if (elem.content.match(/^https?:\/\//) === null) {
                $post.append($('<p class="btn-sm btn-block">')
                        .text(elem.content));
            } else if (elem.content.match(/youtube\.com\/embed\//) === null) {
                const parsedUrl = elem.content.split('/');
                $post.append($('<a class="btn btn-sm btn-block text-left bg-light text-strong">')
                        .attr('href', elem.content)
                        .append($('<img class="w-25">')
                                .attr('src', `${parsedUrl[0]}//${parsedUrl[2]}/favicon.ico`)
                                .attr('onerror', 'this.src="/img/generic-site.png"'))
                        .append("  ")
                        .append(parsedUrl[2]));
            } else {
                $post.append($('<div class="embed-responsive embed-responsive-16by9">')
                        .append($('<iframe class="embed-responsive-item" allowfullscreen>')
                                .attr('src', elem.content)));
            }
        }
        $post.append($('<div class="row m-1">')
                .append($('<div class="col-2 text-center btn">')
                        .attr('onclick', `like(this,${elem.id})`)
                        .addClass(elem.myReaction === "like" ? "text-primary" : "")
                        .append($('<i class="fas fa-thumbs-up">'))
                        .append(elem.likes))
                .append($('<div class="col-2 text-center btn">')
                        .attr('onclick', `dislike(this,${elem.id})`)
                        .addClass(elem.myReaction === "dislike" ? "text-danger" : "")
                        .append($('<i class="fas fa-thumbs-down">'))
                        .append(elem.dislikes))
                .append($('<div class="col-8 text-center text-muted btn-sm">')
                        .append($('<i class="fas fa-clock">'))
                        .append(new Date(elem.created).toLocaleString('bs-ba'))));
    });
}

function fill_latest_area() {
    const $post_area = $('#latest-area');
    $post_area.empty();
    $.each(latest, function (idx, elem) {
        const $post = $('<div class="border-bottom border-top mb-5">');
        $post_area.append($post);
        if (elem.type === 'event') {
            $post.append($('<div class="card">')
                    .append($('<img class="card-img-top">')
                            .attr('src', `/event-pic/${elem.picture}`))
                    .append($('<div class="btn btn-lg btn-block border-bottom m-0">')
                            .append($('<div class="float-left">')
                                    .text(elem.title))
                            .append($('<div class="float-right">')
                                    .text(elem.category)))
                    .append($('<div class="card-body">')
                            .append($('<p class="card-text">')
                                    .text(elem.description))
                            .append($('<small>')
                                    .text(`Date: ${new Date(elem.held).toLocaleString('bs-ba')}`))));
        } else {
            if (elem.type === 'user_post') {
                $post.append($('<a class="btn btn-sm btn-block text-left border-bottom bg-light">')
                        .attr('href', `/user/${elem.authorUsername}`)
                        .append($('<img class="profile-img rounded-circle">')
                                .attr('src', `/img/${elem.authorUsername}.jpg?dummy=${rand}`)
                                .attr('onerror', 'this.src="/img/generic-user.png"'))
                        .append(elem.author));
            } else {
                $post.append($('<p class="btn btn-sm btn-block text-left border-bottom bg-light">')
                        .append($('<i class="fas fa-newspaper h3">'))
                        .append(elem.title));
            }
            if (elem.content.match(/^https?:\/\//) === null) {
                $post.append($('<p class="btn-sm btn-block">')
                        .text(elem.content));
            } else if (elem.content.match(/youtube\.com\/embed\//) === null) {
                const parsedUrl = elem.content.split('/');
                $post.append($('<a class="btn btn-sm btn-block text-left bg-light text-strong">')
                        .attr('href', elem.content)
                        .append($('<img class="w-25">')
                                .attr('src', `${parsedUrl[0]}//${parsedUrl[2]}/favicon.ico`)
                                .attr('onerror', 'this.src="/img/generic-site.png"'))
                        .append("  ")
                        .append(parsedUrl[2]));
            } else {
                $post.append($('<div class="embed-responsive embed-responsive-16by9">')
                        .append($('<iframe class="embed-responsive-item" allowfullscreen>')
                                .attr('src', elem.content)));
            }
        }
        $post.append($('<div class="row m-1">')
                .append($('<div class="col-2 text-center btn">')
                        .attr('onclick', `like(this,${elem.id})`)
                        .addClass(elem.myReaction === "like" ? "text-primary" : "")
                        .append($('<i class="fas fa-thumbs-up">'))
                        .append(elem.likes))
                .append($('<div class="col-2 text-center btn">')
                        .attr('onclick', `dislike(this,${elem.id})`)
                        .addClass(elem.myReaction === "dislike" ? "text-danger" : "")
                        .append($('<i class="fas fa-thumbs-down">'))
                        .append(elem.dislikes))
                .append($('<div class="col-8 text-center text-muted btn-sm">')
                        .append($('<i class="fas fa-clock">'))
                        .append(new Date(elem.created).toLocaleString('bs-ba'))));
    });
}

function find_posts() {
    $.get({
        url: '/api/posts',
        success: function ($response) {
            popular = $response.sort(function (a, b) {
                return b.likes + a.dislikes - a.likes - b.dislikes;
            }).slice(0, 5);
            latest = $response.sort(function (a, b) {
                return new Date(a.created) < new Date(b.created);
            });
            fill_popular_area();
            fill_latest_area();
            setTimeout(find_posts, 30000);
        }
    });
}

function like(node, id) {
    $.post({
        url: '/api/reactions/like',
        contentType: 'application/json',
        data: JSON.stringify({
            postId: id
        }),
        success: function ($response) {
            const $node = $(node);
            const counter = $node.text();
            if ($response === 1) {
                $node.addClass('text-primary');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-up"></i>${Number(counter) + 1}`);
            } else if ($response === -1) {
                $node.removeClass('text-primary');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-up"></i>${Number(counter) - 1}`);
            } else {
                const $nextNode = $node.next();
                const dislikeCounter = $nextNode.text();
                $node.addClass('text-primary');
                $nextNode.removeClass('text-danger');
                $node.empty();
                $nextNode.empty();
                $node.html(`<i class="fas fa-thumbs-up"></i>${Number(counter) + 1}`);
                $nextNode.html(`<i class="fas fa-thumbs-down"></i>${Number(dislikeCounter) - 1}`);
            }
        }
    });
}

function dislike(node, id) {
    $.post({
        url: '/api/reactions/dislike',
        contentType: 'application/json',
        data: JSON.stringify({
            postId: id
        }),
        success: function ($response) {
            const $node = $(node);
            const counter = $node.text();
            if ($response === 1) {
                $node.addClass('text-danger');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-down"></i>${Number(counter) + 1}`);
            } else if ($response === -1) {
                $node.removeClass('text-danger');
                $node.empty();
                $node.html(`<i class="fas fa-thumbs-down"></i>${Number(counter) - 1}`);
            } else {
                const $prevNode = $node.prev();
                const likeCounter = $prevNode.text();
                $node.addClass('text-danger');
                $prevNode.removeClass('text-primary');
                $node.empty();
                $prevNode.empty();
                $node.html(`<i class="fas fa-thumbs-down"></i>${Number(counter) + 1}`);
                $prevNode.html(`<i class="fas fa-thumbs-up"></i>${Number(likeCounter) - 1}`);
            }
        }
    });
}

function find_blog() {
    $.get({
        url: '/api/blog',
        success: function ($response) {
            const $blog_area = $('#blog-area');
            $blog_area.empty();
            $.each($response.sort(function (a, b) {
                return new Date(a.date) < new Date(b.date);
            }), function (idx, elem) {
                $comments = $('<div>');
                $.each(elem.comments, function (id, comm) {
                    $comments.append($('<div class="media mt-3">')
                            .append($('<div class="media-left">')
                                    .append($('<img class="rounded-circle profile-img-sm">')
                                            .attr('src', `/img/${comm.author}.jpg?dummy=${rand}`)
                                            .attr('onerror', 'this.src="/img/generic-user.png"')))
                            .append($('<div class="media-body">')
                                    .append($('<small>')
                                            .text(comm.content))));
                });
                $blog_area.append($('<div class="media mt-3">')
                        .append($('<div class="media-left">')
                                .append($('<img class="rounded-circle profile-img">')
                                        .attr('src', `/img/${elem.author}.jpg?dummy=${rand}`)
                                        .attr('onerror', 'this.src="/img/generic-user.png"')))
                        .append($('<div class="media-body">')
                                .append($('<small>')
                                        .text(elem.content))
                                .append($comments)
                                .append($(`<form action='javascript:new_comment("${elem.id}");'>`)
                                        .append($('<div class="input-group input-group-sm mt-1">')
                                                .append($('<input class="form-control form-control-sm"  placeholder="write comment">')
                                                        .attr('id', `blog-${elem.id}`))
                                                .append($('<div class="input-group-append">')
                                                        .append($('<input type="submit" class="input-group-text bg-light text-dark fas" value="&#xf061;">')))))));
            });
        }
    });
}

function new_topic() {
    const $content = $('#new-topic');
    if ($content.val() === "") {
        $content.addClass('is-invalid');
    } else {
        $.post({
            url: '/api/blog',
            data: $content.val(),
            success: function () {
                $content.val("");
                find_blog();
            }
        });
    }
}

function new_comment(id) {
    console.log(id);
    $.post({
        url: '/api/blog/comment',
        contentType: 'application/json',
        data: JSON.stringify({
            id: id,
            content: $(`#blog-${id}`).val()
        }),
        success: function () {
            find_blog();
        }
    });
}

function find_materials() {
    $.get({
        url: '/material',
        success: function ($response) {
            const $download_zone = $('#download-zone');
            $download_zone.html($response);
        }
    });
}

function change_label() {
    const $label = $('#doc_name');
    const $input = $('#document');
    $label.empty();
    $label.append($('<i class="fas fa-file">'))
            .append(' ')
            .append($input[0].files[0].name);
}