let save_profile_shown = false;
let save_password_shown = false;

function change_label() {
    const $label = $('#photo_name');
    const $input = $('#profile_picture');
    $label.empty();
    $label.append($('<i class="fas fa-image">'))
            .append(' ')
            .append($input[0].files[0].name);
    activate_save_profile();
}

function activate_save_profile() {
    if (save_profile_shown === false) {
        $('#save_profile').removeAttr('hidden');
        save_profile_shown = true;
    }
}

function activate_save_password() {
    if (save_password_shown === false) {
        $('#save_password').removeAttr('hidden');
        save_password_shown = true;
    }
}

function change_password() {
    const $password1 = $('#password1');
    const $password2 = $('#password2');
    const $old_password = $('#old_password');
    if ($password1.val() !== $password2.val()) {
        $password1.addClass('is-invalid');
        $password2.addClass('is-invalid');
    } else {
        $.post({
            url: '/api/password',
            contentType: 'application/json',
            data: JSON.stringify({
                old: $old_password.val(),
                new : $password1.val()
            }),
            statusCode: {
                200: function () {
                    $('#save_password').removeClass('btn-danger')
                            .addClass('btn-success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2500);
                },
                401: function () {
                    $old_password.addClass('is-invalid');
                },
                403: function () {
                    window.location.href = '/login';
                }
            }
        });
    }
}
