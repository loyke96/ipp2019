const rand = Date.now();
let friends = [];
let requests = [];
let others = [];
let faculty = null;

function init() {
    $.get({
        url: '/api/connect/init',
        success: function ($response) {
            const $faculty = $('#faculty');
            friends = $response.friends;
            requests = $response.requests;
            others = $response.others;
            faculty = $response.faculty;
            $faculty.val(faculty);
            fill_requests();
            fill_friends();
            fill_others();
            $faculty.on('change', fill_others);
        }
    });
}

function fill_others() {
    const $faculty = $('#faculty').val();
    const $search_area = $('#search-area');
    $search_area.empty();
    $.each(others.filter(function (o) {
        return o.faculty === Number($faculty);
    }), function (idx, elem) {
        $search_area.append($(`<div class="border-top border-bottom bg-light" id='oth-${elem.username}'>`)
                .append($('<p class="btn-sm btn-block mb-0">')
                        .append($('<img class="rounded-circle profile-img">')
                                .attr('src', `/img/${elem.username}.jpg?dummy=${rand}`)
                                .attr('onerror', 'this.src="/img/generic-user.png"'))
                        .append(elem.name)
                        .append(' ')
                        .append(elem.surname)
                        .append($('<span class="float-right m-1">')
                                .append($('<i class="fas fa-link btn">')
                                        .attr('onclick', `request(${idx})`)))));
    });
}

function fill_requests() {
    const $request_area = $('#request-area');
    $request_area.empty();
    $.each(requests, function (idx, elem) {
        $request_area.append($(`<div class="border-top border-bottom bg-light" id='req-${elem.username}'>`)
                .append($('<p class="btn-sm btn-block mb-0">')
                        .append($('<img class="rounded-circle profile-img">')
                                .attr('src', `/img/${elem.username}.jpg?dummy=${rand}`)
                                .attr('onerror', 'this.src="/img/generic-user.png"'))
                        .append(elem.name)
                        .append(' ')
                        .append(elem.surname)
                        .append($('<span class="float-right m-1">')
                                .append($('<i class="fas fa-check btn">')
                                        .attr('onclick', `accept(${idx})`))
                                .append($('<i class="fas fa-times btn">')
                                        .attr('onclick', `decline(${idx})`)))));
    });
}

function fill_friends() {
    const $friends_area = $('#friends-area');
    $friends_area.empty();
    $.each(friends, function (idx, elem) {
        $friends_area.append($(`<div class="border-top border-bottom bg-light" id='fnd-${elem.username}'>`)
                .append($('<p class="btn-sm btn-block mb-0">')
                        .append($('<img class="rounded-circle profile-img">')
                                .attr('src', `/img/${elem.username}.jpg?dummy=${rand}`)
                                .attr('onerror', 'this.src="/img/generic-user.png"'))
                        .append(elem.name)
                        .append(' ')
                        .append(elem.surname)
                        .append($('<span class="float-right m-1">')
                                .append($('<i class="fas fa-times btn">')
                                        .attr('onclick', `remove_friend(${idx})`)))));
    });
}

function request(id) {
    $.post({
        url: '/api/connect/request',
        data: others[id].username,
        success: function () {
            others.splice(id, 1);
            fill_others();
        }
    });
}

function accept(id) {
    $.post({
        url: '/api/connect/accept',
        data: requests[id].username,
        success: function () {
            friends.push(requests[id]);
            requests.splice(id, 1);
            fill_requests();
            fill_friends();
        }
    });
}

function decline(id) {
    $.post({
        url: '/api/connect/decline',
        data: requests[id].username,
        success: function () {
            others.push(requests[id]);
            requests.splice(id, 1);
            fill_requests();
            fill_others();
        }
    });
}

function remove_friend(id) {
    if (confirm(`Removing ${friends[id].name} ${friends[id].surname} from friends... Are you sure?`)) {
        $.post({
            url: '/api/connect/remove',
            data: friends[id].username,
            success: function () {
                others.push(friends[id]);
                friends.splice(id, 1);
                fill_friends();
                fill_others();
            }
        });
    }
}
