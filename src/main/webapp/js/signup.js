function signup_validation() {
    const $password1 = $('#password1');
    const $password2 = $('#password2');
    if ($password1.val() !== $password2.val()) {
        $password1.addClass('is-invalid');
        $password2.addClass('is-invalid');
    } else {
        const $username = $('#username');
        const $email = $('#email');
        $.post({
            url: '/api/signup/available',
            contentType: 'application/json',
            data: JSON.stringify({
                username: $username.val(),
                email: $email.val()
            }),
            success: function ($response) {
                if (!$response.email) {
                    $email.addClass('is-invalid');
                } else if (!$response.username) {
                    $username.addClass('is-invalid');
                } else {
                    do_signup();
                }
            }
        });
    }
}

function do_signup() {
    $.post({
        url: '/api/signup',
        contentType: 'application/json',
        data: JSON.stringify({
            username: $('#username').val(),
            name: $('#name').val(),
            surname: $('#surname').val(),
            password: $('#password1').val(),
            email: $('#email').val()
        }),
        statusCode: {
            200: function () {
                window.location.href = '/profile';
            }, 400: function () {
                $('#errMsg').removeAttr('hidden');
            }
        }
    });
}

function remove_msg() {
    $('#password1').removeClass('is-invalid');
    $('#password2').removeClass('is-invalid');
    $('#username').removeClass('is-invalid');
    $('#email').removeClass('is-invalid');
}
