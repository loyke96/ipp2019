<%@page import="java.util.List"%>
<%@page import="org.unibl.etf.ip.entity.Faculty"%>
<%@page import="org.unibl.etf.ip.entity.Student"%>
<jsp:useBean id="user_id" scope="session" type="java.lang.Integer"/>
<jsp:useBean id="studentFacade" scope="request" type="org.unibl.etf.ip.facade.StudentFacade"/>
<jsp:useBean id="facultyFacade" scope="request" class="org.unibl.etf.ip.facade.FacultyFacade"/> 

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <title>aluno - profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/brands.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="/js/profile.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">aluno</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/"><i class="fas fa-home"></i> home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/profile"><i class="fas fa-user"></i> profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/connect"><i class="fas fa-link"></i> connect
                            <%int count = studentFacade.numberOfRequests(user_id);
                                if (count != 0) {%>
                            <span class="badge badge-danger"><%=count%></span>
                            <%}%>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout"><i class="fas fa-sign-out-alt"></i> log out</a>
                    </li>
                </ul>
            </div>
        </nav>

        <%
            Student student = studentFacade.find(user_id);
            List<Faculty> faculties = facultyFacade.findAll();
        %>

        <div class="container">
            <form method="post" action="/profile" enctype="multipart/form-data" accept-charset="UTF-8">
                <div class="row">
                    <a class="btn btn-block col-10 border-bottom mb-5" data-toggle="collapse" data-target=".multi-collapse" aria-controls="user_data change_password">
                        <i class="fas fa-user-edit"></i> edit profile
                    </a>
                    <div class="col-2">
                        <input type="submit" class="btn btn-dark btn-block m-1 fas" id="save_profile" value="&#xf0c7; save" hidden> 
                    </div>
                </div>
                <div class="container collapse multi-collapse show" id="user_data">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img class="rounded w-75" src="/img/<%=student.getUsername()%>.jpg?dummy=<%=System.currentTimeMillis()%>" onerror="this.src='/img/generic-user.png'"/>
                            <div class="input-group mt-4 text-left">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input form-control-sm" id="profile_picture" name="profile_picture" accept="image/*" onchange="change_label()">
                                    <label id="photo_name" class="custom-file-label" for="profile_picture"><i class="fas fa-image"></i> change photo</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><i class="fas fa-user-tag"></i> name</label>
                                <input class="form-control form-control-sm" id="name" name="name" value="<%=student.getName()%>" required onchange="activate_save_profile()">
                            </div>
                            <div class="form-group">
                                <label for="surname"><i class="fas fa-user-tag"></i> surname</label>
                                <input class="form-control form-control-sm" id="surname" name="surname" value="<%=student.getSurname()%>" required onchange="activate_save_profile()">
                            </div>
                            <div class="form-group">
                                <label for="interests"><i class="fas fa-book-open"></i> interests</label>
                                <textarea class="form-control form-control-sm" id="interests" name="interests" rows="5" onchange="activate_save_profile()"><%=student.getInterests() != null ? student.getInterests() : ""%></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="faculty"><i class="fas fa-university"></i> faculty</label>
                                <select class="custom-select custom-select-sm" id="faculty" name="faculty" required onchange="activate_save_profile()">
                                    <%for (Faculty f : faculties) {%>
                                    <option value="<%=f.getId()%>"><%=f.getName()%></option>
                                    <%}%>
                                    <%if (student.getFaculty() != null) {%>
                                    <script>
                                        $('#faculty').val(<%=student.getFaculty().getId()%>);
                                    </script>
                                    <%}%>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="study_field"><i class="fas fa-university"></i> field of study</label>
                                <input class="form-control form-control-sm" id="study_field" name="study_field" value="<%=student.getStudyField() != null ? student.getStudyField() : ""%>" required onchange="activate_save_profile()">
                            </div>
                            <div class="form-group">
                                <label for="study_year"><i class="fas fa-user-graduate"></i> year of study</label>
                                <input class="form-control form-control-sm" id="study_year" type="number" min="1" max="7" name="study_year" value="<%=student.getStudyYear() != null ? student.getStudyYear() : 1%>" required onchange="activate_save_profile()">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form action="javascript:change_password()">
                <div class="row">
                    <a class="btn btn-block col-10 border-bottom text-danger mb-5" data-toggle="collapse" data-target=".multi-collapse" aria-controls="change_password user_data">
                        <i class="fas fa-key"></i> change password
                    </a>
                    <div class="col-2">
                        <input type="submit" class="btn btn-block btn-danger m-1 fas" id="save_password" value="&#xf0c7; save" hidden>
                    </div>
                </div>
                <div class="container collapse multi-collapse" id="change_password">
                    <div class="row">
                        <div class="col-md-4 offset-md-2">
                            <div class="alert alert-secondary m-3" role="alert">
                                <i class="fas fa-shield-alt"></i> security notice:
                                <ul>
                                    <li>use a minimum password length of 8 or more characters</li>
                                    <li>include lowercase and uppercase alphabetic characters, numbers and symbols</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="old_password"><i class="fas fa-key"></i> old password</label>
                                <input type="password" class="form-control form-control-sm" id="old_password" name="old_password" required>
                                <div class="invalid-feedback">wrong password</div>
                            </div>
                            <div class="form-group">
                                <label for="password1"><i class="fas fa-key"></i> new password</label>
                                <input type="password" class="form-control form-control-sm" id="password1" name="password1" required onchange="activate_save_password()">
                                <div class="invalid-feedback">password doesn't match</div>
                            </div>
                            <div class="form-group">
                                <label for="password2"><i class="fas fa-key"></i> confirm password</label>
                                <input type="password" class="form-control form-control-sm" id="password2" name="password2" required onchange="activate_save_password()">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
