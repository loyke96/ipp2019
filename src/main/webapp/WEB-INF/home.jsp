<%@page import="org.unibl.etf.ip.entity.Student"%>
<%@page import="org.unibl.etf.ip.facade.StudentFacade"%>
<jsp:useBean id="user_id" scope="session" type="java.lang.Integer"/>
<jsp:useBean id="studentFacade" scope="request" type="org.unibl.etf.ip.facade.StudentFacade"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <title>aluno</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/brands.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="/js/home.js"></script>
    </head>
    <body onload="find_posts();find_blog();find_materials();">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">aluno</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/"><i class="fas fa-home"></i> home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/profile"><i class="fas fa-user"></i> profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/connect"><i class="fas fa-link"></i> connect
                            <%int count = studentFacade.numberOfRequests(user_id);
                                if (count != 0) {%>
                            <span class="badge badge-danger"><%=count%></span>
                            <%}%>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout"><i class="fas fa-sign-out-alt"></i> log out</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-md-3 h-100">
                    <div class="scrollable border-left border-right">
                        <p class="text-center btn btn-block">
                            <i class="fas fa-users"></i> connections
                        </p>
                        <%for (Student s : studentFacade.findConnections(user_id)) {%>
                        <div class="border-top border-bottom bg-light text-center">
                            <a href="/user/<%=s.getUsername()%>" class="btn btn-sm btn-block">
                                <img src="/img/<%=s.getUsername()%>.jpg?dummy=<%=System.currentTimeMillis()%>" onerror="this.src='/img/generic-user.png'" class="rounded-circle profile-img">
                                <%=s.getName()%>
                                <%=s.getSurname()%>
                            </a>
                        </div>
                        <%}%>
                    </div>
                </div>
                <div class="col-md-5 h-100">
                    <div class="scrollable">
                        <form method="post" action="/home">
                            <div class="input-group">
                                <textarea class="form-control form-control-sm" id="content" name="content" rows="1" placeholder="say something ..." required></textarea>
                                <div class="input-group-append">
                                    <input type="submit" class="input-group-text bg-light text-dark fas" value="&#xf061;">
                                </div>
                            </div>
                        </form>
                        <p class="text-center btn btn-block mt-3">
                            <i class="fas fa-star"></i> popular posts
                        </p>
                        <div id="popular-area">

                        </div>
                        <p class="text-center btn btn-block mt-5">
                            <i class="fas fa-edit"></i> latest posts
                        </p>
                        <div id="latest-area">

                        </div>
                    </div>
                </div>
                <div class="col-md-4 h-100">
                    <div class="h-50 scrollable">
                        <p class="text-center btn btn-block">
                            <i class="fas fa-blog"></i> blog
                        </p>
                        <div class="input-group input-group-sm">
                            <textarea class="form-control form-control-sm" id="new-topic" rows="1" placeholder="start new topic"></textarea>
                            <div class="input-group-append">
                                <input type="button" class="input-group-text bg-light text-dark fas" value="&#xf061;" onclick="new_topic()">
                            </div>
                        </div>
                        <div id="blog-area">

                        </div>
                    </div>
                    <div class="h-50 scrollable">
                        <p class="text-center btn btn-block">
                            <i class="fas fa-download"></i> download zone
                        </p>
                        <form method="post" action="/material" enctype="multipart/form-data">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input form-control-sm" id="document" name="document" onchange="change_label()">
                                    <label id="doc_name" class="custom-file-label" for="document"><i class="fas fa-file"></i> document</label>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input class="form-control form-control-sm" id="description" name="description" placeholder="description">
                                <div class="input-group-append">
                                    <input type="submit" class="input-group-text bg-light text-dark fas" value="&#xf061;">
                                </div>
                            </div>
                        </form>
                        <div id="download-zone">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
