<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <title>aluno - log in</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/brands.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">aluno</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/login"><i class="fas fa-sign-in-alt"></i> log in</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/signup"><i class="fas fa-user-plus"></i> sign up</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container mt-5">
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="card card-signin my-5 bg-light">
                        <div class="card-body">
                            <h5 class="text-center card-title"><i class="fas fa-sign-in-alt"></i> log in</h5>
                            <form method="post" action="/login">
                                <div class="form-group">
                                    <input name="username" class="form-control form-control-sm fas" placeholder="&#xf007; username" required autofocus>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control form-control-sm fas" placeholder="&#xf084; password" required>
                                </div>
                                <button class="btn btn-sm btn-dark btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> log in</button>
                                <%String error = (String) request.getAttribute("error");
                                    if (error != null) {
                                %>
                                <div class="d-flex justify-content-center">
                                    <span class="badge badge-danger mt-1">
                                        <i class="fas fa-times"></i> <%=error%>
                                    </span>
                                </div>
                                <%
                                    }
                                %>
                            </form>
                            <h6 class=" mt-5 text-center">new to aluno? <a href="/signup"><i class="fas fa-user-plus"></i> sign up</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
