<%@page import="org.unibl.etf.ip.entity.Faculty"%>
<jsp:useBean id="facultyFacade" scope="request" type="org.unibl.etf.ip.facade.FacultyFacade"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <title>aluno - connect</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/brands.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="/js/connect.js"></script>
    </head>
    <body onload="init()">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">aluno</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/"><i class="fas fa-home"></i> home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/profile"><i class="fas fa-user"></i> profile</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/connect"><i class="fas fa-link"></i> connect</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout"><i class="fas fa-sign-out-alt"></i> log out</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <p class="text-center btn btn-block">
                        <i class="fas fa-search"></i> search
                    </p>
                    <select class="custom-select custom-select-sm mb-3" id="faculty">
                        <%for (Faculty f : facultyFacade.findAll()) {%>
                        <option value="<%=f.getId()%>"><%=f.getName()%></option>
                        <%}%>
                    </select>
                    <div id="search-area">

                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-center btn btn-block">
                        <i class="fas fa-comment"></i> requests
                    </p>
                    <div id="request-area">

                    </div>
                </div>
                <div class="col-md-4">
                    <p class="text-center btn btn-block">
                        <i class="fas fa-users"></i> friends
                    </p>
                    <div id="friends-area">

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
