<%@page import="org.unibl.etf.ip.entity.UserPost"%>
<jsp:useBean id="user" scope="request" type="org.unibl.etf.ip.entity.Student"/>
<jsp:useBean id="count" scope="request" type="java.lang.Integer"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <title>aluno - <%=user.getName()%> <%=user.getSurname()%></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/brands.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="/js/user.js"></script>
    </head>
    <body onload="get_posts()">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">aluno</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/"><i class="fas fa-home"></i> home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/profile"><i class="fas fa-user"></i> profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/connect"><i class="fas fa-link"></i> connect
                            <%if (count != 0) {%>
                            <span class="badge badge-danger"><%=count%></span>
                            <%}%>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout"><i class="fas fa-sign-out-alt"></i> log out</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 border-left border-right">
                    <div class="text-center mb-6">
                        <img class="rounded w-75 mb-3" src="/img/<%=user.getUsername()%>.jpg?dummy=<%=System.currentTimeMillis()%>" onerror="this.src='/img/generic-user.png'"/>
                        <p class="btn btn-lg btn-block"><i class="fas fa-user"></i> <%=user.getName()%> <%=user.getSurname()%></p>
                    </div>
                    <table class="table">
                        <tr>
                            <td><i class="fas fa-at"></i> email:</td>
                            <td><%=user.getEmail()%></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-university"></i> faculty:</td>
                            <td><%=user.getFaculty().getName()%></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-university"></i> field of study:</td>
                            <td><%=user.getStudyField()%></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-user-graduate"></i> year of study:</td>
                            <td><%=user.getStudyYear()%></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4 border-left border-right">
                    <p class="text-center btn-block">
                        <i class="fas fa-edit"></i> latest posts
                    </p>
                    <div id="post-area">

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
