<%@page import="org.unibl.etf.ip.entity.Material"%>
<jsp:useBean id="materialFacade" scope="request" type="org.unibl.etf.ip.facade.MaterialFacade"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%for (Material m : materialFacade.findAll()) {%>
<div class="border-top border-bottom bg-light">
    <div class="container">
        <div class="row">
            <div class="col-2">
                <a href="/user/<%=m.getStudent().getUsername()%>">
                    <img src="/img/<%=m.getStudent().getUsername()%>.jpg?dummy=<%=System.currentTimeMillis()%>" onerror="this.src='/img/generic-user.png'" class="rounded-circle profile-img">
                </a>
            </div>
            <div class="col-10">
                <a href="/doc/<%=m.getDocument()%>" class="btn btn-sm btn-block text-left">
                    <%=m.getDescription()%>
                </a>
            </div>
        </div>
    </div>
</div>
<%}%>
